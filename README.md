# GrandCore School

![Логотип не загрузился](https://gitlab.com/grandcore/gc-school/-/raw/master/README.files/school-logo.png)

Цель данного проекта - создать на основе нашего движка для систем совместной работы над проектами [EDem](https://gitlab.com/grandcore/gc-edem) открытую образовательную платформу, где силами сообщества будут создаваться программы для обучения разным навыкам.

В первую очередь, мы будем работать над наборами карточек [Anki](https://apps.ankiweb.net/), и в дальнейшем перенесём всё это на онлайн платформу.

В данный момент [готова](https://gitlab.com/grandcore/gc-school/-/tree/master/content/spanish) заготовка на основе частотного словоря испанского языка. Основная задача сейчас - написать [приложение](https://gitlab.com/grandcore/gc-school/-/tree/master/app) для парсинга данных.

Чат проекта: https://t.me/joinchat/WOqWW6843XYND-Zb
